import "../styles/loading.css";
export default function Loading() {
    // You can add any UI inside Loading, including a Skeleton.
    return (
      <div className=" w-screen h-screen grid place-items-center place-content-center">
        <div className="loadingio-spinner-double-ring-hhskvwontko">
          <div className="ldio-xwbz8zp9dzo">
            <div></div>
            <div></div>
            <div>
              <div></div>
            </div>
            <div>
              <div></div>
            </div>
          </div>
        </div>
      </div>
    )
  }